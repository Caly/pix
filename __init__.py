#!/usr/bin/python3
# coding: utf-8

from flask import Flask
import os

app = Flask(__name__)
app.config.update(
    BASE_DIR = os.path.abspath(os.path.dirname(__file__)),
    DEBUG = True,
    SECRET_KEY = 'still in dev'
)

import pix.views
