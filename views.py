#!/usr/bin/python3
# coding: utf-8

from flask import Flask, render_template, url_for
from pix import app

def to_json(data):
    return json.dumps(data, default=json_util.default)

@app.route('/pix')
def home():
    return render_template('home.html')
