
var MTH_POST = "POST";
var MTH_GET = "GET";
var MTH_DEL = "DELETE";
var API_LOCATION = "http://127.0.0.1:5000/api/";

function getXMLHttpRequest(){
	var xhr = null;
	if(window.XMLHttpRequest || window.ActiveXObject) {
		if(window.ActiveXObject){
			try{
				xhr = new ActiveXObject("Msxml12.XMLHTTP");
			} catch(e) {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
		} else {
			xhr = new XMLHttpRequest();
		}
		return xhr;
	} else {
		alert("Votre navigateur ne gère pas XMLHttpRequest...");
		return null;
	}
}

function cancelCurrent(xhr){
	if(xhr && xhr.readyState != 0){
		xhr.abort();
	}
	return false;
}

function cancelNew(xhr){
	if(xhr && xhr.readyState != 0){
		return true;
	}
	return false;
}

var xhr = null;

var sendFile = function(request) {
    if(xhr == null)
        var xhr = getXMLHttpRequest();
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)){
            if(request["loadingElement"] != null)
                request["loadingElement"].style.display = "none";
            if(request["disableElement"] != null)
                request["disableElement"].disabled = false;
            request["callback"](xhr.responseText);
            xhr = null;
        } else if(xhr.readyState < 4 && request["loadingElement"] != null){
            request["loadingElement"].style.display = "inline";
            if(request["disableElement"] != null)
                request["disableElement"].disabled = true;
        }
    };
    
    var formData = new FormData();
    for(var i in request["params"]){
            formData.append(i,encodeURIComponent(request["params"][i]));
    }
    if("oldfile" in request["params"]){
        formData.append("oldfile",request["params"]["oldfile"]);
    }
    
    if("who" in request["params"]){
        formData.append("who",request["params"]["who"]);

    }

    formData.append("file",request["params"]["data"]);
    for(var i in request["files"]) {
        formData.append(i,request["files"][i]);
    }

    xhr.open("POST",request["url"],true);
    xhr.send(formData);
};

var sendRequest = function(request){
	if(request["security"] == null){
		request["security"] = cancelNew;
	}
	if(request["security"](xhr)) return;
	if(xhr == null)
		var xhr = getXMLHttpRequest();
	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)){
			if(request["loadingElement"] != null)
					request["loadingElement"].style.display = "none";
			if(request["disableElement"] != null)
				request["disableElement"].disabled = false;
			request["callback"](decodeURIComponent(xhr.responseText));
			xhr = null;
		} else if(xhr.readyState < 4 && request["loadingElement"] != null){
			request["loadingElement"].style.display = "inline";
			if(request["disableElement"] != null)
				request["disableElement"].disabled = true;
		}
	};
	
	var paramStr = null;
	var firstElem = true;
        if(request["method"] == "GET"){
            for(var p in request["params"]){
                    if(!firstElem){
                            paramStr += "&";
                    } else {
                            firstElem = false;
                            paramStr = "";
                    }
                    paramStr += p + "=" + encodeURIComponent(request["params"][p]);
            }
        } else {
            paramStr = new FormData();
            for(var p in request["params"]){
                paramStr.append(p,encodeURIComponent(request["params"][p]));
            }
        }
	if(request["method"] == "POST" || request["method"] == "DELETE"){
		xhr.open(request["method"],request["url"],true);
	} else{
		if(paramStr != null)
			request["url"] += "?" + paramStr;
		paramStr = null;
		xhr.open(request["method"],request["url"],true);
	}
	xhr.send(paramStr);
};