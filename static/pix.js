
function init() {

    var send_button = document.getElementById('send_file');
    var file_input = document.getElementById('selected_file');
    var ALLOWED_EXTENSIONS = ['jpg', 'png', 'gif'];

    file_input.onclick = function() {
        this.value = null;
    }

    file_input.onchange = function() {
        var allowed = false;
        var filename = this.value.split('\\');
        filename = filename[filename.length-1];
        var file_extension = filename.split('.');
        file_extension = file_extension[file_extension.length-1];

        ALLOWED_EXTENSIONS.forEach(function(extension) {
            if(file_extension == extension) {
                allowed = true;
            }
        })

        function result_send_file(xhrReturn) {
            var response = JSON.parse(xhrReturn)
            console.debug(response);
        }

        var request_send_file = {
            'callback': result_send_file,
            'method': 'POST',
            'url': API_LOCATION + 'pictures/up',
            'params': {

            },
            'files': {
                'file': file
            },
            'loadingElement': null,
            'disableElement': null,
            'security': null
        };

        if(allowed) {
            var file = file_input.files[0];
            sendFile(request_send_file);
        } 

    }

    send_button.addEventListener('click', function(e) {
        e.preventDefault();
        
    }, false);

    function result_latest_pictures(xhrReturn) {
        var result = JSON.parse(xhrReturn);
        var latest_pictures = result['pictures'];

        latest_pictures.forEach(function(picture) {
            var latest_pictures_div = document.getElementById('latest_pictures');
            var thumbnail = "<div class=\"col-xs-6 col-md-3\">" +
                            "<a href=\"#\" class=\"thumbnail\">" +
                            "<img src=\"http://127.0.0.1:5000/api/pictures/thumbnail/" + picture['picture'] + "\" alt=\"" + picture['name'] + "\"\\>" +
                            "</a>" +
                            "</div>";
            latest_pictures_div.innerHTML+= thumbnail;
        });
    }

    var request_latest_pictures = {
        'callback' : result_latest_pictures,
        'method' : 'GET',
        'url' : API_LOCATION + 'pictures/latest_pictures',
        'loadingElement' : null,
        'disableElement' : null,
        'security' : null
    };

    sendRequest(request_latest_pictures);
}

window.onload = init;